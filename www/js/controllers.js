angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('DashboardCtrl', function($rootScope, $scope, $state, $ionicPopup, $ionicPopover, Debts, Credits) {
  $rootScope.$on('data:listChanged', function() {
    $scope.updateList();
  });

  $scope.debts = Debts.getFromLocal();
  $scope.credits = Credits.getFromLocal();

  $scope.updateList = function() {
    $scope.debts = Debts.getFromLocal();
    $scope.credits = Credits.getFromLocal();
  };

  $scope.goAddDebt = function() {
    $scope.popover.hide();
    $state.go('app.addDebt',{} ,{reload: true});
  };

  $scope.goAddCredit = function() {
    $scope.popover.hide();
    $state.go('app.addCredit',{} ,{reload: true});
  };


  // .fromTemplate() method
  var template = '<ion-popover-view> \
    <ion-header-bar> \
      <h1 class="title">Tambahkan</h1> \
    </ion-header-bar> \
    <ion-content> \
      <button class="button button-full button-small button-positive" ng-click="goAddDebt()"> \
        Hutang \
      </button> \
      <button class="button button-full button-small button-positive" ng-click="goAddCredit()"> \
        Piutang \
      </button> \
    </ion-content> \
  </ion-popover-view>';

  $scope.popover = $ionicPopover.fromTemplate(template, {
      scope: $scope
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };


  // Triggered on a button click, or some other target
  /*$scope.showPopup = function() {
    $scope.data = {}

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<input type="password" ng-model="data.wifi">',
      title: 'Tambahkan',
      //subTitle: 'Please use normal things',
      scope: $scope,
      buttons: [
        { text: 'Hutang' },
        {
          text: '<b>Piutang</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.data.wifi) {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {
              return $scope.data.wifi;
            }
          }
        }
      ]
    });
    myPopup.then(function(res) {
    console.log('Tapped!', res);
    });
  };*/

})

.controller('DebtDetailCtrl', function($scope, $state, $stateParams, $ionicPopup, Debts) {
  debts = Debts.getFromLocal();
  $scope.debtDetail = debts[$stateParams.debtId];

  // A confirm dialog
  $scope.showConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Yakin sudah lunas?',
      //template: 'Yakin sudah lunas?',
      cancelText: 'Belum',
      okText: 'Lunas'
    });

    confirmPopup.then(function(res) {
      if(res) {
        Debts.delete($stateParams.debtId);
        $scope.$emit('data:listChanged');
        $state.go('app.dashboard');
      }
    });
  };
})

.controller('CreditDetailCtrl', function($scope, $state, $stateParams, $ionicPopup, Credits) {
  credits = Credits.getFromLocal();
  $scope.creditDetail = credits[$stateParams.creditId];

  // A confirm dialog
  $scope.showConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Yakin sudah lunas?',
      //template: 'Yakin sudah lunas?',
      cancelText: 'Belum',
      okText: 'Lunas'
    });

    confirmPopup.then(function(res) {
      if(res) {
        Credits.delete($stateParams.creditId);
        $scope.$emit('data:listChanged');
        $state.go('app.dashboard');
      }
    });
  };
})

.controller('AddDebtCtrl', function($scope, $state, $cordovaDatePicker, Debts) {

  /*document.addEventListener("deviceready", function () {
    $cordovaContacts.find({filter: '',multiple:true}).then(function(allContacts) { //omitting parameter to .find() causes all contacts to be returned
      $scope.allContacts = allContacts;
    });
  });

  $scope.friends = [{name:'John', phone:'555-1276'},
             {name:'Mary', phone:'800-BIG-MARY'},
             {name:'Mike', phone:'555-4321'},
             {name:'Adam', phone:'555-5678'},
             {name:'Julie', phone:'555-8765'},
             {name:'Juliette', phone:'555-5678'}];*/

  // DATE
  var dayBahasa = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  var monthBahasa = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

  var options = {
    date: new Date(),
    mode: 'date', // or 'time'
    minDate: new Date() - 10000,
    allowOldDates: true,
    allowFutureDates: false,
    doneButtonLabel: 'DONE',
    doneButtonColor: '#F2F3F4',
    cancelButtonLabel: 'CANCEL',
    cancelButtonColor: '#000000'
  };

  $scope.initDueDateParam = null;

  $scope.addDueDate = function() {
    $cordovaDatePicker.show(options).then(function(date){
      $scope.dueDate = dayBahasa[date.getDay()] + ', ' + date.getDate() + ' ' + monthBahasa[date.getMonth()] + ' ' + date.getFullYear()
    });
    $scope.initDueDateParam = 1;
  };

  // ADD DEBT
  $scope.addDebt = function(debt) {
    Debts.add({
      amount: debt.amount,
      creditor: debt.creditor,
      duedate: $scope.dueDate,
      note: debt.note
    });
    $scope.$emit('data:listChanged');
    $state.go('app.dashboard');
  };
})

.controller('AddCreditCtrl', function($scope, $state, $cordovaDatePicker, Credits) {
  // DATE
  var dayBahasa = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  var monthBahasa = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

  var options = {
    date: new Date(),
    mode: 'date', // or 'time'
    minDate: new Date() - 10000,
    allowOldDates: true,
    allowFutureDates: false,
    doneButtonLabel: 'DONE',
    doneButtonColor: '#F2F3F4',
    cancelButtonLabel: 'CANCEL',
    cancelButtonColor: '#000000'
  };

  $scope.initDueDateParam = null;

  $scope.addDueDate = function() {
    $cordovaDatePicker.show(options).then(function(date){
      $scope.dueDate = dayBahasa[date.getDay()] + ', ' + date.getDate() + ' ' + monthBahasa[date.getMonth()] + ' ' + date.getFullYear()
    });
    $scope.initDueDateParam = 1;
  };

  // ADD DEBT
  $scope.addCredit = function(credit) {
    Credits.add({
      amount: credit.amount,
      debtor: credit.creditor,
      duedate: $scope.dueDate,
      note: credit.note
    });
    $scope.$emit('data:listChanged');
    $state.go('app.dashboard');
  };
});
