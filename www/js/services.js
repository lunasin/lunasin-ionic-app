angular.module('starter.services', [])

.factory('Debts', function($window) {
  return {
    getFromLocal: function() {
      return JSON.parse($window.localStorage['debts'] || '[]');
    },
    setToLocal: function(value) {
      $window.localStorage['debts'] = JSON.stringify(value);
    },
    add: function(value) {
      var debts = this.getFromLocal();
      debts.push(value);
      this.setToLocal(debts);
    },
    delete: function(debtId) {
      var debts = this.getFromLocal();
      debts.splice(debtId, 1);
      this.setToLocal(debts);
    }
  };
    /*{ amount: 12300,
      creditor: 'Aaa',
      note: 'hutang mendoan' },
    { amount: 132000,
      creditor: 'Baa',
      note: 'hutang mendoan 2' },
    { amount: 45000,
      creditor: 'Caa',
      note: 'hutang mendoan 3' },
    { amount: 32000,
      creditor: 'Daa',
      note: 'hutang mendoan 4' },
    { amount: 1300000,
      creditor: 'Eaa',
      note: 'hutang mendoan 5' },
    { amount: 12000,
      creditor: 'Faa',
      note: 'hutang mendoan 6' }
  ];*/
})

.factory('Credits', function($window) {
  return {
    getFromLocal: function() {
      return JSON.parse($window.localStorage['credits'] || '[]');
    },
    setToLocal: function(value) {
      $window.localStorage['credits'] = JSON.stringify(value);
    },
    add: function(value) {
      var credits = this.getFromLocal();
      credits.push(value);
      this.setToLocal(credits);
    },
    delete: function(creditId) {
      var credits = this.getFromLocal();
      credits.splice(creditId, 1);
      this.setToLocal(credits);
    }
  };
/*
  [
    { amount: 1200,
      id: 1,
      debtor: 'Aaa',
      note: 'hutang tempe' },
    { amount: 1320,
      id: 2,
      debtor: 'Baa',
      note: 'hutang tempe 2' },
    { amount: 4500,
      id: 3,
      debtor: 'Caa',
      note: 'hutang tempe 3' },
    { amount: 3200,
      id: 4,
      debtor: 'Daa',
      note: 'hutang tempe 4' },
    { amount: 1300,
      id: 5,
      debtor: 'Eaa',
      note: 'hutang tempe 5' },
    { amount: 1200,
      id: 6,
      debtor: 'Faa',
      note: 'hutang tempe 6' },
  ];*/
});
